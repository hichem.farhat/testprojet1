import { GestionService } from './gestion.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ListeComponent } from './liste/liste.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AddComponent } from './add/add.component';
import { EdituserComponent } from './edituser/edituser.component';
import { NavComponent } from './nav/nav.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListeComponent,
    AddComponent,
    EdituserComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    Ng2SearchPipeModule ,
    ReactiveFormsModule,


    FormsModule,
    NgxWebstorageModule.forRoot()
  ],
  providers: [GestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
