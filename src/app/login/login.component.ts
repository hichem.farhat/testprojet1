import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GestionService } from '../gestion.service';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  response:any = { "token":""}

  @LocalStorage()   userconnect:any;

  constructor(private ser:GestionService, private route:Router, private local:LocalStorageService) { }

  ngOnInit() {


  }

  verification(request){
    this.ser.login(request).subscribe(
      data=>{
            this.response=data.body;
            let token=this.response.token;
            localStorage.setItem("token",token)
            this.ser.saveToken(token);

            this.route.navigate(['/liste'])
      },
      err=> {

      }
    )

  }

}
