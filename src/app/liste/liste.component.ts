import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestionService } from '../gestion.service';


@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {
user:any
  users:any=[];
  searchtext:any
  p:number=1
  roles:any=[];



  constructor(private ser:GestionService, private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getalluser()



  }
  getalluser(){this.ser.getuser().subscribe(
    data=>{

        this.users=data;
        console.log(this.users)
    },
    errr =>{

    }
)}


  supp(id:number){
    this.ser.deleteuser(id).subscribe(
      data=>{


        this.ngOnInit();
      },
      err=>{

       }
    )
  }





}
