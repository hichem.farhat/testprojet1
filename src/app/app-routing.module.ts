import { AddComponent } from './add/add.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListeComponent } from './liste/liste.component';
import { LoginComponent } from './login/login.component';
import { EdituserComponent } from './edituser/edituser.component';
import { AuthService } from './auth.service';


const routes: Routes = [

  {path:"", component:LoginComponent},
  {path:"liste", component:ListeComponent,canActivate:[AuthService]},
  {path:"login", component:LoginComponent},
  {path:"add", component:AddComponent,canActivate:[AuthService]},
  {path: 'edituser/:id', component:EdituserComponent,canActivate:[AuthService] },


];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
