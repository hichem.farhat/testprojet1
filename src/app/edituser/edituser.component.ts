import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { GestionService } from '../gestion.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {
  id:number;
  user=null
  constructor(private route:Router, private local:LocalStorageService,private ser:GestionService,private ac:ActivatedRoute) {
    this.id=this.ac.snapshot.params['id'];
   }

  ngOnInit() {
    this.getuser()
  }
  getuser(){
    this.ser.getuserr(this.id).subscribe(
      data => { this.user=data
        console.log(data) },
      err => { console.log})}
      updatuser() {
        this.ser.UPDATE(this.user.id, this.user)
          .subscribe(
            data=>{this.route.navigate(['/liste'])},
  erre=>{}
  );
      }

}
