import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  @LocalStorage() userconnect:any;
  constructor(private local:LocalStorageService) { }

  ngOnInit() {
  }
  deconnexion(){
    this.local.clear("userconnect");
  }

}
