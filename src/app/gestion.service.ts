import { Injectable } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GestionService {
  username:string;
  token:string;
  @LocalStorage() userconnect;

  constructor(private http:HttpClient) { }
  getuser(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/auth/getall/",{headers:headers})

  }
  save(user:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/auth/save/",user,{headers:headers})

  }
  getuserr(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/auth/getuser/"+id,{headers:headers})

  }
  UPDATE(id:number,user:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put("http://127.0.0.1:8080/auth/update/"+id,user,{headers:headers})

  }
  deleteuser(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/auth/deleteuser/"+id,{headers:headers})

  }
  login(request){
    return this.http.post("http://127.0.0.1:8080/auth/login",request,{observe:'response'})
    }
  getUserByUserName(username:string){
    let hs=new HttpHeaders({"Authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/auth/user/"+username, {headers:hs})
  }

  saveToken(token){
    let helper=new JwtHelperService();
    this.token=token;
    let dec=helper.decodeToken(token);
    console.log(dec);

    this.username=dec.sub;

    this.getUserByUserName(this.username).subscribe(
      data=> { this.userconnect=data},
      err=>{}
    )
  }
 }
